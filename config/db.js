const Sequelize = require("sequelize");

const db = new Sequelize({
    dialect: "sqlite",
    storage: "restuarant.sqlite3",
});

module.exports = db;
