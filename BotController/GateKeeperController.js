const menu = require('./Data/menu');
const db = require('../config/db');
const Menus = require('../Models/Menu');
const Cart = require('../Models/Cart');
const Sequelize = require('sequelize');


module.exports.inquiryMessage = (msg, userId, platform) =>{
    if(msg.toLowerCase() === "menu"){
        return getMenu().then((res) => {
            return res;
        });
    }

    if(msg.toLowerCase().includes("order")){
        return addCart(msg, userId, platform).then((res) => {
            return res;
        });
    }

    if(msg.toLowerCase().includes("checkout")){
        return checkout(msg, userId).then((res) => {
            return res;
        });
    }
    return new Promise((res) => {
        res(`${msg}`);
    });
};

checkout = (item, userId) => {
    return Cart.findOne({
        where: {
            userId: userId,
            isComplete: false
        }
    }).then((cart) => {
        if(!cart){
            return getMenu().then((res) => {
                return "No Item. \n" + res;
            });
        }else{
            let cartObj = JSON.parse(cart.items);
            let checkoutObj = [];
            let totalPrice = 0.0;
            let primAll = [];
            for (let key in cartObj) {
                if (cartObj.hasOwnProperty(key)) {
                    primAll.push(Menus.findOne({
                        where: {
                            $col: Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), key.toLowerCase())
                        }
                    }).then((menuObj) => {
                        checkoutObj.push({name: key, price: menuObj.price * parseInt(cartObj[key]), unit: cartObj[key]});
                        totalPrice += menuObj.price * parseInt(cartObj[key]);
                    })
                )
                }
            }

            cart.update({isComplete: true}).then((res) => {
                console.log("Done" + res);
            });

            return Promise.all(primAll)
                .then(() => {
                   let invoice = "Your order. \n";
                   for(let i = 0;i < checkoutObj.length; i++){
                       console.log(checkoutObj[i]);
                       invoice += `${i + 1}. ${(checkoutObj[i]).name} (${checkoutObj[i].unit} : ${(checkoutObj[i]).price.toLocaleString()}) \n`
                   }
                    invoice += `Total Price: ${totalPrice.toLocaleString()}`;
                    return invoice;
                });
        }
    }).catch((error) => {
        console.error(error);
    });
};


module.exports.orderHandler = (item) => {
    let unit = 1;
    let hasNumber = /\d/;
    if(hasNumber.test(item)){
        if(!isNaN(item.substring(item.lastIndexOf(" ") + 1 , item.length))){
            unit = item.substring(item.lastIndexOf(" ") + 1 , item.length);
        }
        return {name : item.substring(item.indexOf(" ") + 1, item.lastIndexOf(" ") === item.indexOf(" ")? item.length: item.lastIndexOf(" ")), unit : unit};
    }else{
        return {name : item.substring(item.indexOf(" ") + 1, item.length), unit : unit};
    }

};

addCart = (order, userId, platform) => {
    let obj = this.orderHandler(order);
    const name = obj.name;
    const unit = obj.unit;
    return Menus.findOne({
        where: {
            $col: Sequelize.where(Sequelize.fn('lower', Sequelize.col('name')), name.toLowerCase())
        }
    }).then((itemMenu) => {
        if(itemMenu) {
            return Cart.findOne({
                where : {
                    userId: userId,
                    isComplete: false
                }
            }).then((cart) => {
                if (!cart) {
                    let cartObj = {};
                    cartObj[itemMenu.name] = parseInt(unit);
                    return Cart.create({
                        platform: platform, userId: userId, items: JSON.stringify(cartObj), isComplete: false
                    }).then((res) => {
                        return `You ordered ${itemMenu.name} ${unit}.`;
                    }).catch((err) => {
                        console.error(err);
                    });
                } else {
                    let cartObj = JSON.parse(cart.items);
                    if (cartObj[itemMenu.name] != null) {
                        cartObj[itemMenu.name] = parseInt(cartObj[itemMenu.name]) + parseInt(unit);
                    } else {
                        cartObj[itemMenu.name] = parseInt(unit);
                    }

                    return cart.update({items: JSON.stringify(cartObj)}).then((res) => {
                        return `You ordered ${itemMenu.name} ${cartObj[itemMenu.name]}.`;
                    });
                }
            }).catch((error) => {
                console.error(error);
                return new Promise((res) => {
                    res(`Cannot find ${itemMenu.name} on Menu.`);
                });
            });
        }else{
            return new Promise((res) => {
                res(`Cannot find ${name} on Menu.`);
            });
        }
    }).catch((err) => {
        console.log(err);
    });
};

getMenu = () => {
    const menu = getGetAllMenu().then((res) => {
        let menuStr = "Menu.\n";
        res.map((item, index) => {
            menuStr += `${index + 1}. ${item.name} (${item.price} ${item.currency}) \n`
        });
        return menuStr;
    });
    return menu;
};

getGetAllMenu = (res) => {
    return Menus.findAll().then((result) => {
        return result;
    });
};
