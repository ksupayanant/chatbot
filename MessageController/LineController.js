const request = require('request-promise');
const config = require('config');
const gateKeeper = require('../BotController/GateKeeperController');

const LINE_MESSAGING_API = config.get('lineMessageAPIUrl');
const LINE_TOKEN = config.get('lineAccessToken');

const LINE_HEADER = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${LINE_TOKEN}`
};

module.exports.reply = (reqBody) => {
    gateKeeper.inquiryMessage(reqBody.events[0].message.text, reqBody.events[0].source.userId, "LINE")
        .then((res) => {
            return request({
                method: 'POST',
                uri: `${LINE_MESSAGING_API}/reply`,
                headers: LINE_HEADER,
                body: JSON.stringify({
                    replyToken: reqBody.events[0].replyToken,
                    messages: [
                        {
                            type: 'text',
                            text: res
                        }
                    ]
                })
            });
    }).catch((err) => {
        console.error(err);
    });
};

