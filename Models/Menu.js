const sequelize = require("../config/db");
const { DataTypes } = require("sequelize");

const Menu = sequelize.define("Menu", {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true,
    },
    price: {
        type: DataTypes.REAL,
        allowNull: false,
    },
    currency: {
        type: DataTypes.STRING,
        allowNull: true,
    },
});

module.exports = Menu;
