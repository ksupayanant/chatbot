var express = require('express');
var router = express.Router();

var lineController = require('../MessageController/LineController');

router.post('/webhook', function(req, res, next) {
   if(req.body.events.length === 0) {
     res.setHeader('Content-Type', 'application/json');
     res.sendStatus(200).json();
   }

  if(req.body.events[0].message.type !== 'text'){
    return;
  }
  lineController.reply(req.body);
});

module.exports = router;
