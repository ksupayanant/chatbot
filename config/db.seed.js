const db = require("./db");
const Cart = require("../Models/Cart");
const Menu = require("../Models/Menu");


const seed = async () => {
    await db.sync({ force: true , logging: console.log });

    Menu.bulkCreate([
        {name: "Pork Shop", description: "", price: 60.00, currency: "THB"},
        {name: "Ribeye", description: "", price: 80.00, currency: "THB"},
        {name: "Spaghetti", description: "", price: 40.00, currency: "THB"},
        {name: "Omlet", description: "", price: 35.00, currency: "THB"},
        {name: "Corn Soup", description: "", price: 40.00, currency: "THB"},
        {name: "French fries", description: "", price: 60.00, currency: "THB"}
    ]).then((res) => {
        console.log(res);

    }).catch((err) => {
        console.error(err);
    });

};
seed();
