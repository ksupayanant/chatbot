const sequelize = require("../config/db");
const { DataTypes } = require("sequelize");

const Cart = sequelize.define("Cart", {
    platform: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    userId: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    items: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    isComplete: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
    },
});

module.exports = Cart;
