const chai = require('chai');
const expect = chai.expect;
const GateKeeper = require('../BotController/GateKeeperController');

describe('GateKeeper', () => {
    it('Type menu should return Text', () => {
        GateKeeper.inquiryMessage("menu","FakeUserId", "TEST").then((menu) => {
            expect(menu).inquiryMessage("menu","","").to.contain("menu");
        });
    });

    it('Type order should return Item and unit', () => {
        const item = {name: "pork shop", unit: 1};
        expect(GateKeeper.orderHandler("order pork shop")).to.eql(item);
    });
});
